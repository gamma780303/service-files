import {UploadedFile} from 'express-fileupload';
import {config} from "../env/env";
import {uuid} from "uuidv4";
import {S3} from 'aws-sdk'

const allowedFileExtensions = ['jpg', 'jpeg', 'png'];

if (!config.s3Config) {
    throw new Error('No config for s3');
}

const bucket = new S3({
    region: config.s3Config.s3Region,
    accessKeyId: config.s3Config.s3AccessKey,
    secretAccessKey: config.s3Config.s3SecretKey
});

const fileNameBuilder = (fileName: string): string => {
    const fileExtension = fileName.split('.').pop();

    return `${uuid()}.${fileExtension}`;
}

export default class S3Service {
    public static async uploadFile(file: UploadedFile): Promise<string> {
        if(!allowedFileExtensions.includes(file.name.split('.').pop())) {
            throw new Error(`image extension is not supported ${file}`);
        }
        const {name, data, mimetype} = file;
        const uploadPath = fileNameBuilder(name);
        //@ts-ignore
        const dataBuffer = Buffer.from(data.data);
        const uploadedFile = await bucket.upload({
            Bucket: config.s3Config.s3Name,
            Body: dataBuffer,
            Key: uploadPath,
            ContentType: mimetype
        }).promise();
        return uploadedFile.Location;
    }

    public static async uploadBuffer(key: string, buffer: Buffer): Promise<string> {
        const uploadPath = fileNameBuilder(key);
        const mimetype = `image/${key}`;
        //@ts-ignore
        const optimizedBuffer = Buffer.from(buffer.data);
        const uploadedFile = await bucket.upload({
            Bucket: config.s3Config.s3Name,
            Body: optimizedBuffer,
            Key: uploadPath,
            ContentType: mimetype
        }).promise();
        return uploadedFile.Location;
    }
}
