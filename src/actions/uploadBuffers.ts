import { Action, Payload } from 'package-app';
import {
    FilesActionName,
    UploadBuffersPayload,
    UploadBuffersResult,
    UploadFilesPayload,
    UploadFilesResult
} from "package-types";
import * as fs from "fs";
import S3Service from "../services/s3Service";

export default new class UploadFiles implements Action{
    getName(): string{
        return FilesActionName.UploadBuffers;
    }

    getValidationSchema(): any {
        return {
            buffers: {
                type: 'object',
            }
        };
    }

    async execute(payload: Payload<UploadBuffersPayload>): Promise<UploadBuffersResult> {
        const { buffers } = payload.params;
        const result: UploadBuffersResult = {fileKeys: {}};
        try {
            await Promise.all(Object.keys(buffers).map( async (key) => {
                const name = await S3Service.uploadBuffer(key, buffers[key]);
                result.fileKeys[key] = name;
            }));
            return result;
        } catch (err) {
            throw new Error(`cannot upload ${err}`);
        }
    }
}
