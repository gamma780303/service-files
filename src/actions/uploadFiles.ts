import { Action, Payload } from 'package-app';
import { FilesActionName, UploadFilesPayload, UploadFilesResult } from "package-types";
import * as fs from "fs";
import S3Service from "../services/s3Service";

export default new class UploadFiles implements Action{
    getName(): string{
        return FilesActionName.UploadFiles;
    }

    getValidationSchema(): any {
        return {
            files: {
                type: 'object',
            }
        };
    }

    async execute(payload: Payload<UploadFilesPayload>): Promise<UploadFilesResult> {
        const { files } = payload.params;
        const result: UploadFilesResult = {fileKeys: {}};
        try {
            await Promise.all(Object.keys(files).map( async (key) => {
                const name = await S3Service.uploadFile(files[key]);
                result.fileKeys[key] = name;
            }));
        } catch (err) {
            throw new Error(`cannot upload ${err}`);
        }
        return result;
    }
}
